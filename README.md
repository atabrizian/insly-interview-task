Insly Interview Task
======================

To run the server:  
- cd [$PROJECT_DIRECTORY]  
- mkdir .data  
- sudo chmod -R 777 .data  
- ./run.sh  

Tasks are arranged in this order:
- Task1.php  
    To execute the file:
    - cd ./docker
    - docker-compose exec workspace /bin/sh -c "php Task1.php"
- Task2.php  
    - http://localhost:8090/Task2.php  
- Task3: check folder content
