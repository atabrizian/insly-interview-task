<?php

function printCharList(array $charList): void {
    foreach ($charList as $char) {
        echo $char;
    }

    echo PHP_EOL;
}

$charList = ['A', 'r', 'a', 's', 'h'];
printCharList($charList);
