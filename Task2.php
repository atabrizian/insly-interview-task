<?php

/**
 * skip creating a custom micro-framework (sanitization, routing, configuration loading, DI, request/response conversion)
 */

set_include_path(__DIR__ . DIRECTORY_SEPARATOR);
spl_autoload_extensions('.php');
spl_autoload_register(function ($class) {
    include_once str_replace('\\', '/', $class) . '.php';
});

use Task2\Controller\IndexController;
use Task2\Model\InsuranceApplication;

define('COMMISSION_PERCENTAGE', 17.0); // instead of configuration file

session_start();

$controller = new IndexController();

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    $csrf = $_POST['csrf'];

    if ($_SESSION['csrf'] !== $csrf) {
        $status = 400;
        $data = 'Bad request! Refresh the page and try again.';
    } else {
        try {
            $application = new InsuranceApplication();
            $application->setInstalmentCount(intval($_POST['instalments']));
            $application->setTaxPercentage(floatval($_POST['tax']));
            $application->setValue(floatval($_POST['value']));

            $application->setCreatedAt(
                (new DateTime())->setTimestamp(sprintf('%.0f', floatval($_POST['created_at'])))
            ); // user time

            $response = $controller->submitAction($application);
            $status = $response['status'];
            $data = $response['data'];
        } catch (Throwable $exception) {
            $status = 400;
            $data = 'Bad request! Invalid date!';
        }
    }

    header('Content-Type: application/json');
    header('Status Code: ' . $status);
    echo json_encode(['data' => $data]);
} else {
    header('Content-Type: text/html');
    header('Status Code: ' . 200);
    echo $controller->getAction()['data'];
}
