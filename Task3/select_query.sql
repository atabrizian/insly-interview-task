with translation_data as (
    select employee_id, JSON_OBJECTAGG(entity, json_data) as translation
    from (
             select foreign_key                                             as employee_id,
                    entity,
                    json_object(locale, JSON_OBJECTAGG(t.field, t.content)) AS json_data
             from translations t
             where foreign_key = 1
             group by entity, locale) as entity_data
    group by employee_id
),
     experiences as (
         select employee_id,
                json_arrayagg(json_object('company_name', eex.company_name, 'job_title', eex.job_title, 'start_date',
                                          eex.start_date, 'end_date', eex.end_date)) AS job_experience
         from employee_experiences eex
         where employee_id = 1
         group by id
     ),
     educations as (
         select employee_id,
                json_arrayagg(json_object('degree', d.name, 'university_name', eedu.university_name, 'start_date',
                                          eedu.start_date, 'end_date', eedu.end_date)) AS education
         from employee_educations eedu
                  inner join degrees d on eedu.degree_id = d.id
         where employee_id = 1
         group by eedu.id
     ),
     contact_informations as (
         select employee_id,
                json_arrayagg(json_object('email', eci.email, 'phone', eci.phone, 'address',
                                          eci.address)) AS contact_information
         from employee_contact_informations eci
         where employee_id = 1
         group by id
     ),
     introductions as (
         select employee_id, json_object('introduction', ei.introduction) AS introduction
         from employee_introductions ei
         where employee_id = 1
     )
SELECT e.*, experiences.job_experience, educations.education, introductions.introduction, translation_data.translation
from employees e
         inner join experiences on experiences.employee_id = e.id
         inner join educations on educations.employee_id = e.id
         inner join introductions on introductions.employee_id = e.id
         inner join translation_data on translation_data.employee_id = e.id
where e.id = 1;
