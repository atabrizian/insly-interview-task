<?php

namespace Task2\Normalizer;

interface NormalizerInterface
{
    public function normalize(object $object): array;
}
