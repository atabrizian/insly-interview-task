<?php

namespace Task2\Normalizer;

use Task2\Model\InsuranceApplication;

class InsuranceApplicationNormalizer implements NormalizerInterface
{
    /**
     * @param object|InsuranceApplication $object
     * @return array
     */
    public function normalize(object $object): array
    {
        return [
            'value' => $object->getValue(),
            'taxPercentage' => $object->getTaxPercentage(),
            'instalmentCount' => $object->getInstalmentCount(),
            'createdAt' => $object->getCreatedAt()->format('Y-m-d H:i:s'),
        ];
    }
}
