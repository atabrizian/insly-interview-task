<?php

namespace Task2\Normalizer;

use Task2\Model\Instalment;
use Task2\Model\Insurance;

class InsuranceNormalizer implements NormalizerInterface
{
    /**
     * @param object|Insurance $object
     * @return array
     */
    public function normalize(object $object): array
    {
        $applicationNormalizer = new InsuranceApplicationNormalizer();
        $instalmentNormalizer = new InstalmentNormalizer();

        return [
            'application' => $applicationNormalizer->normalize($object->getApplication()),
            'commissionPercentage' => $object->getCommissionPercentage(),
            'policyPercentage' => $object->getPolicy()->getPercentage(),
            'basePremium' => number_format($object->getBasePremium(), 2),
            'commission' => number_format($object->getCommission(), 2),
            'tax' => number_format($object->getTax(), 2),
            'instalments' => array_map(function (Instalment $instalment) use ($instalmentNormalizer) {
                return $instalmentNormalizer->normalize($instalment);
            }, $object->getInstalments()),
            'total' => number_format($object->getTotal(), 2),
        ];
    }
}
