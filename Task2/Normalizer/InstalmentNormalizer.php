<?php

namespace Task2\Normalizer;

use Task2\Model\Instalment;

class InstalmentNormalizer implements NormalizerInterface
{
    /**
     * @param object|Instalment $object
     * @return array
     */
    public function normalize(object $object): array
    {
        return [
            'number' => $object->getNumber(),
            'basePremium' => number_format($object->getBasePremium(), 2),
            'commission' => number_format($object->getCommission(), 2),
            'tax' => number_format($object->getTax(), 2),
            'total' => number_format($object->getTotal(), 2),
        ];
    }
}
