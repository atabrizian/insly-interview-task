<?php

namespace Task2\Controller;

use Task2\InsuranceCalculator;
use Task2\Model\InsuranceApplication;
use Task2\Normalizer\InsuranceNormalizer;
use Throwable;

class IndexController
{
    public function getAction()
    {
        $csrf = sha1(microtime());
        $_SESSION['csrf'] = $csrf;

        return [
            'status' => 200,
            'data' => str_replace(
                '{{ csrf }}',
                $csrf,
                file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . '../View/view.php')
            ),
        ];
    }

    public function submitAction(InsuranceApplication $application)
    {
        try {
            $calculator = new InsuranceCalculator(COMMISSION_PERCENTAGE);
            $insurance = $calculator->calculate($application);

            $data = (new InsuranceNormalizer())->normalize($insurance);
            $status = 200;
        } catch (Throwable $exception) {
            $data = 'Internal Server Error';
            $status = 500;
        }

        return ['status' => $status, 'data' => $data];
    }
}
