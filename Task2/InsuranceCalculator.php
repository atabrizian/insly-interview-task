<?php

namespace Task2;

use Task2\Model\Instalment;
use Task2\Model\Insurance;
use Task2\Model\InsuranceApplication;
use Task2\Model\Policy\PolicyFactory;
use Task2\Model\Policy\PolicyInterface;

class InsuranceCalculator
{
    private float $commissionPercentage;

    public function __construct(float $commissionPercentage)
    {
        $this->commissionPercentage = $commissionPercentage;
    }

    public function calculate(InsuranceApplication $application): Insurance
    {
        $policy = PolicyFactory::createPolicy($application->getCreatedAt());

        return $this->createInsurance($application, $policy);
    }

    private function createInsurance(InsuranceApplication $application, PolicyInterface $policy): Insurance
    {
        $insurance = new Insurance();
        $insurance->setApplication($application);
        $insurance->setPolicy($policy);
        $insurance->setCommissionPercentage($this->commissionPercentage);

        $insurance->setBasePremium($policy->calculateBasePrice($application->getValue()));
        $insurance->setCommission($this->calculateCommission($insurance->getBasePremium()));
        $insurance->setTax($this->calculateTax($insurance->getBasePremium(), $application->getTaxPercentage()));

        $basePremiumPerInstalment = round($insurance->getBasePremium() / $application->getInstalmentCount(), 2);
        $commissionPerInstalment = round($insurance->getCommission() / $application->getInstalmentCount(), 2);
        $taxPerInstalment = round($insurance->getTax() / $application->getInstalmentCount(), 2);

        $instalments = [];
        for ($i = 1; $i <= $application->getInstalmentCount(); $i++) {
            $instalment = new Instalment($i, $basePremiumPerInstalment, $commissionPerInstalment, $taxPerInstalment);

            if ($i === 1) {
                $this->setRemainderOnFirstInstalmentIfAvailable($insurance, $instalment);
            }

            $instalments[] = $instalment;
        }

        $insurance->setInstalments($instalments);

        return $insurance;
    }

    private function calculateCommission(float $basePrice): float
    {
        return round(($basePrice * $this->commissionPercentage) / 100, 2);
    }

    private function calculateTax(float $basePrice, float $taxPercentage): float
    {
        return round(($basePrice * $taxPercentage) / 100, 2);
    }

    private function setRemainderOnFirstInstalmentIfAvailable(Insurance $insurance, Instalment $instalment): void
    {
        $instalmentCount = $insurance->getApplication()->getInstalmentCount();

        $basePremiumPerInstalmentRemainder = round(
            $insurance->getBasePremium() - ($instalment->getBasePremium() * $instalmentCount),
            2
        );
        $instalment->setBasePremium($instalment->getBasePremium() + $basePremiumPerInstalmentRemainder);

        $commissionPerInstalmentRemainder = round(
            $insurance->getCommission() - ($instalment->getCommission() * $instalmentCount),
            2
        );
        $instalment->setCommission($instalment->getCommission() + $commissionPerInstalmentRemainder);

        $taxPerInstalmentRemainder = round(
            $insurance->getTax() - ($instalment->getTax() * $instalmentCount),
            2
        );
        $instalment->setTax($instalment->getTax() + $taxPerInstalmentRemainder);
    }
}
