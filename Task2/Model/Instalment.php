<?php

namespace Task2\Model;

class Instalment
{
    private int $number;
    private float $basePremium;
    private float $commission;
    private float $tax;

    public function __construct(int $number, float $basePremium, float $commission, float $tax)
    {
        $this->number = $number;
        $this->basePremium = $basePremium;
        $this->commission = $commission;
        $this->tax = $tax;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    public function getBasePremium(): float
    {
        return $this->basePremium;
    }

    public function setBasePremium(float $basePremium): void
    {
        $this->basePremium = $basePremium;
    }

    public function getCommission(): float
    {
        return $this->commission;
    }

    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    public function getTax(): float
    {
        return $this->tax;
    }

    public function setTax(float $tax): void
    {
        $this->tax = $tax;
    }

    public function getTotal(): float
    {
        return $this->getBasePremium() + $this->getCommission() + $this->getTax();
    }
}
