<?php

namespace Task2\Model;

use DateTimeInterface;
use Task2\Model\Policy\PolicyInterface;

class InsuranceApplication
{
    private float $value;
    private float $taxPercentage;
    private int $instalmentCount;
    private DateTimeInterface $createdAt;

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function getTaxPercentage(): float
    {
        return $this->taxPercentage;
    }

    /**
     * @param float $taxPercentage
     */
    public function setTaxPercentage(float $taxPercentage): void
    {
        $this->taxPercentage = $taxPercentage;
    }

    /**
     * @return int
     */
    public function getInstalmentCount(): int
    {
        return $this->instalmentCount;
    }

    /**
     * @param int $instalmentCount
     */
    public function setInstalmentCount(int $instalmentCount): void
    {
        $this->instalmentCount = $instalmentCount;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     */
    public function setCreatedAt(DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
