<?php

namespace Task2\Model;

use Task2\Model\Policy\PolicyInterface;

class Insurance
{
    private InsuranceApplication $application;
    private float $commissionPercentage;
    private PolicyInterface $policy;
    private float $basePremium;
    private float $commission;
    private float $tax;

    /**
     * @var Instalment[]
     */
    private array $instalments;

    /**
     * @return InsuranceApplication
     */
    public function getApplication(): InsuranceApplication
    {
        return $this->application;
    }

    /**
     * @param InsuranceApplication $application
     */
    public function setApplication(InsuranceApplication $application): void
    {
        $this->application = $application;
    }

    /**
     * @return float
     */
    public function getCommissionPercentage(): float
    {
        return $this->commissionPercentage;
    }

    /**
     * @param float $commissionPercentage
     */
    public function setCommissionPercentage(float $commissionPercentage): void
    {
        $this->commissionPercentage = $commissionPercentage;
    }

    /**
     * @return PolicyInterface
     */
    public function getPolicy(): PolicyInterface
    {
        return $this->policy;
    }

    /**
     * @param PolicyInterface $policy
     */
    public function setPolicy(PolicyInterface $policy): void
    {
        $this->policy = $policy;
    }

    /**
     * @return float
     */
    public function getBasePremium(): float
    {
        return $this->basePremium;
    }

    /**
     * @param float $basePremium
     */
    public function setBasePremium(float $basePremium): void
    {
        $this->basePremium = $basePremium;
    }

    /**
     * @return float
     */
    public function getCommission(): float
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return float
     */
    public function getTax(): float
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     */
    public function setTax(float $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return Instalment[]
     */
    public function getInstalments(): array
    {
        return $this->instalments;
    }

    /**
     * @param Instalment[] $instalments
     */
    public function setInstalments(array $instalments): void
    {
        $this->instalments = $instalments;
    }

    public function getTotal(): float
    {
        return $this->getBasePremium() + $this->getCommission() + $this->getTax();
    }
}