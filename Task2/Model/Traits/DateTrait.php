<?php

namespace Task2\Model\Traits;

use DateTime;
use DateTimeInterface;

trait DateTrait
{
    public function generateDateTimeByWeekDayAndTime(
        DateTimeInterface $currentDateTime,
        int $dayOfWeek,
        int $hour,
        int $minute = 0
    ): DateTime
    {
        $dateTime = (new DateTime())->setTimestamp($currentDateTime->getTimestamp());

        $currentDayOfWeek = $currentDateTime->format('w');

        $daysDiff = $currentDayOfWeek - $dayOfWeek;
        if ($daysDiff !== 0) {
            $dateTime->modify($daysDiff . ' days');
        }

        $dateTime->setTime($hour, $minute);

        return $dateTime;
    }
}
