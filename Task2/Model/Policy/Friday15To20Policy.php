<?php

namespace Task2\Model\Policy;

use DateTimeInterface;
use Task2\Model\DateRange;

class Friday15To20Policy extends AbstractPolicy
{
    protected float $percentage = 13;

    public function isApplicable(DateTimeInterface $dateTime): bool
    {
        $validDateRange = new DateRange(
            $this->generateDateTimeByWeekDayAndTime($dateTime, 5, 15),
            $this->generateDateTimeByWeekDayAndTime($dateTime, 5, 20)
        );

        return $validDateRange->getStartDate() <= $dateTime && $dateTime <= $validDateRange->getEndDate();
    }
}
