<?php

namespace Task2\Model\Policy;

use Task2\Model\Traits\DateTrait;

abstract class AbstractPolicy implements PolicyInterface
{
    use DateTrait;

    protected float $percentage = 11.0;

    public function calculateBasePrice(float $value): float
    {
        return round(($value * $this->percentage) / 100, 2);
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->percentage;
    }
}
