<?php

namespace Task2\Model\Policy;

use DateTimeInterface;

interface PolicyInterface
{
    /**
     * Check whether current policy is applicable in a chosen point of time and date
     *
     * @param DateTimeInterface $dateTime
     * @return bool
     */
    public function isApplicable(DateTimeInterface $dateTime): bool;

    /**
     * Calculate "base price" based on Insurance value
     *
     * @param float $value
     * @return float
     */
    public function calculateBasePrice(float $value): float;

    /**
     * Percentage used for calculation
     *
     * @return float
     */
    public function getPercentage(): float;
}
