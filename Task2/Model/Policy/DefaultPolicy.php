<?php

namespace Task2\Model\Policy;

use DateTimeInterface;

class DefaultPolicy extends AbstractPolicy
{
    public function isApplicable(DateTimeInterface $dateTime): bool
    {
        return true;
    }
}
