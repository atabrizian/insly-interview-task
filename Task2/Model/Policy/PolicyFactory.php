<?php

namespace Task2\Model\Policy;

use DateTimeInterface;

class PolicyFactory
{
    /**
     * Decide which policy to use and return instance
     *
     * @param DateTimeInterface $dateTime
     * @return PolicyInterface
     */
    public static function createPolicy(DateTimeInterface $dateTime): PolicyInterface
    {
        $friday15To20Policy = new Friday15To20Policy();

        return $friday15To20Policy->isApplicable($dateTime) ? $friday15To20Policy : new DefaultPolicy();
    }
}
