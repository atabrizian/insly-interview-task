function createTHeadElement(data) {
    var thead = document.createElement('thead');
    var titleColHeader = document.createElement('th');
    thead.appendChild(titleColHeader);

    var policyColHeadr = document.createElement('th')
    policyColHeadr.innerText = 'Policy';
    thead.appendChild(policyColHeadr);

    var numberOfInstalment = data.instalments.length;
    for (i = 1; i <= numberOfInstalment; i++) {
        var thTitle = i + ' Instalment';
        var th = document.createElement('th');
        th.innerText = thTitle;
        thead.appendChild(th);
    }

    return thead;
}

function createTDElement(text) {
    var td = document.createElement('td');
    td.innerText = text;

    return td;
}

function createTRElement(rowData, isTotalRow = false) {
    var tr = document.createElement('tr');

    var counter = 0;
    rowData.forEach(function (item) {
        tr.appendChild(createTDElement(item));
    })

    return tr;
}

function createTBodyElement(data) {
    var propertyToTitleMap = {
        'basePremium': 'Base Premium (' + data.policyPercentage + '%)',
        'commission': 'Commission (' + data.commissionPercentage + '%)',
        'tax': 'Tax (' + data.application.taxPercentage + '%)',
        'total': 'Total'
    };

    var tbody = document.createElement('tbody');

    var valueRowData = ['Value', data.application.value];
    data.instalments.forEach(function () {
        valueRowData.push('');
    });
    var valueTR = createTRElement(valueRowData);
    tbody.appendChild(valueTR);

    for (var propertyName in propertyToTitleMap) {
        var rowData = [
            propertyToTitleMap[propertyName],
            data[propertyName],
        ];
        data.instalments.forEach(function (item) {
            rowData.push(item[propertyName]);
        });

        var tr = createTRElement(rowData);
        tbody.appendChild(tr);
    }

    return tbody;
}


function createInsuranceTable(data) {
    var table = document.createElement('table');

    var thead = createTHeadElement(data);
    table.appendChild(thead);

    var tbody = createTBodyElement(data);
    table.appendChild(tbody);

    return table;
}

$(document).ready(function () {
    $('form').on('submit', function (event) {
        event.preventDefault();

        var form = $(this);
        var formData = form.serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;

            return obj;
        }, {});
        formData['created_at'] = (new Date()).getTime() / 1000;

        var url = form.attr('action');

        $.ajax({
            method: 'POST',
            url: url,
            data: formData,
            success: function (data) {
                var resultDiv = $('.result');
                resultDiv.empty();
                resultDiv.html(createInsuranceTable(data.data));
            },
            error: function (data) {
                alert(data.data);
            }
        })

        return false;
    });
});
