<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task 2 - Car Insurance Policy Calculator </title>
    <link rel="stylesheet" href="./Task2/View/style.css">
    <script src="./Task2/View/jquery-3.5.1.min.js"></script>
    <script src="./Task2/View/app.js" type="text/javascript"></script>
</head>
<body>
<div class="form">
    <form action="Task2.php" id="calculator">
        <input type="hidden" name="csrf" value="{{ csrf }}">
        <div class="row">
            <label for="value">
                <div class="label">Value:</div>
                <input type="number" name="value" id="value" min="100" max="100000" required value="10000.0" tabindex="1" autofocus>
            </label>
        </div>
        <div class="row">
            <label for="tax">
                <div class="label">Tax:</div>
                <input type="number" name="tax" id="tax" min="0" max="100" required value="10">&percnt;
            </label>
        </div>
        <div class="row">
            <label for="instalments">
                <div class="label">Instalments:</div>
                <input type="number" name="instalments" id="instalments" min="1" max="12" required value="2">
            </label>
        </div>
        <div class="row">
            <label for="calculate">
                <div></div>
                <input type="submit" id="calculate" name="calculate" value="Calculate"/>
            </label>
        </div>
    </form>
</div>
<div class="result">
</div>
</body>
</html>
